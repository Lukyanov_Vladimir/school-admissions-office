package sample.controllers;

import constants.Constants;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class RemoveRecordController implements Constants {

    @FXML
    private TextField numRecord;

    @FXML
    private Button removeBtn;

    private String tableName;

    private DataBaseEditorController dbec;

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setDbec(DataBaseEditorController dbec) {
        this.dbec = dbec;
    }

    @FXML
    private void initialize() {
        removeBtn.setOnAction(actionEvent -> {
            try {
                DATA_BASE.removeRow(tableName, Integer.parseInt(numRecord.getText()));
            } catch (SQLException e) {
                e.printStackTrace();
            }

            dbec.update();
        });
    }
}
