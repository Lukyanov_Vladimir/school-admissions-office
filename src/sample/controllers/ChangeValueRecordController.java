package sample.controllers;

import constants.Constants;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import java.sql.SQLException;
import java.util.ArrayList;

public class ChangeValueRecordController implements Constants {

    @FXML
    private ComboBox<String> columnName;

    @FXML
    private TextField numRecord;

    @FXML
    private TextField newValue;

    @FXML
    private Button changeBtn;

    private String tableName;

    private DataBaseEditorController dbec;

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setDbec(DataBaseEditorController dbec) {
        this.dbec = dbec;
    }

    @FXML
    private void initialize() {
        changeBtn.setOnAction(actionEvent -> {
            try {
                DATA_BASE.changeCell(tableName, columnName.getValue(), newValue.getText(), Integer.parseInt(numRecord.getText()));
            } catch (SQLException e) {
                e.printStackTrace();
            }

            dbec.update();
        });
    }

    private ArrayList<String> getColumnsNames() throws SQLException {
        return DATA_BASE.getColumnNames(tableName, DATA_BASE.countColumns(tableName));
    }

    public void start() {
        try {
            ArrayList<String> columsNames = getColumnsNames();
            columnName.setValue(columsNames.get(0));
            columnName.setItems(FXCollections.observableArrayList(columsNames));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}