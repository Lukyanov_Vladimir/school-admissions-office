package sample.controllers;

import constants.Constants;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.sql.SQLException;
import java.util.ArrayList;

public class AddRecordController implements Constants {

    @FXML
    private VBox vBox;

    private ArrayList<TextField> textFields;

    private String tableName;

    private Button addBtn;

    private DataBaseEditorController dbec;

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setDbec(DataBaseEditorController dbec) {
        this.dbec = dbec;
    }

    @FXML
    private void initialize() {
        textFields = new ArrayList<>();

        addBtn = new Button("Добавить");
        addBtn.setFont(Font.font(17));

        addBtn.setOnAction(actionEvent -> {
            try {
                DATA_BASE.addRow(tableName, getValues());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            dbec.update();
        });
    }

    public void generateForm() throws SQLException {
        ArrayList<String> columnNames = DATA_BASE.getColumnNames(tableName, DATA_BASE.countColumns(tableName));
        ArrayList<Label> labels = createLabels(columnNames);
        createTextFields(columnNames.size());

        for (int i = 0; i < columnNames.size(); i++) {
            vBox.getChildren().add(labels.get(i));
            vBox.getChildren().add(textFields.get(i));
        }

        VBox vbox2 = new VBox();
        vbox2.setPrefHeight(50);
        vBox.getChildren().addAll(vbox2, addBtn);
    }

    private ArrayList<Label> createLabels(ArrayList<String> columnNames) {
        ArrayList<Label> labels = new ArrayList<>();
        for (String columnName : columnNames) {
            Label label = new Label();
            label.setText(columnName);
            label.setTextAlignment(TextAlignment.LEFT);
            label.setFont(Font.font(17));
            labels.add(label);
        }

        return labels;
    }

    private void createTextFields(int columsCount) {
        for (int i = 0; i < columsCount; i++) {
            TextField textField = new TextField();
            textField.setAlignment(Pos.CENTER);
            textFields.add(textField);
        }
    }

    private ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();

        for (TextField textField : textFields) {
            values.add(textField.getText());
        }

        return values;
    }
}
