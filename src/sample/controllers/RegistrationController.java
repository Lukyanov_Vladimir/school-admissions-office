package sample.controllers;

import constants.Constants;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

public class RegistrationController implements Constants {

    @FXML
    private TextField logField;

    @FXML
    private PasswordField passField;

    @FXML
    private PasswordField verifyPassField;

    @FXML
    private Button regBtn;

    private Stage regStage;

    public void setStage(Stage stage) {
        this.regStage = stage;
    }

    private EventHandler<WindowEvent> eventHandler = e -> {
        try {
            DATA_BASE.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    };

    public EventHandler<WindowEvent> getEventHandler() {
        return eventHandler;
    }

    @FXML
    public void initialize() {
        regBtn.setCursor(Cursor.HAND);

        regBtn.setOnAction(event -> {
            try {
                DATA_BASE.connect();
                String login = logField.getText();
                String password = passField.getText();
                String verifyPass = verifyPassField.getText();
                if (registration(login, password, verifyPass)) {
                    info("Регистрация прошла успешно");
                    close();
                }
            } catch (SQLIntegrityConstraintViolationException e) {
                info("Такое имя пользователя уже существует");

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void info(String str) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Информация");
        alert.setHeaderText(null);
        alert.setContentText(str);
        alert.showAndWait();
    }

    public boolean registration(String login, String password, String verifyPassword) throws SQLException {

        if (password.equals(verifyPassword)) {
            DATA_BASE.requestForData("INSERT INTO `" + TABLE_USERS + "` (`login`, `password`) VALUES ('" + login + "', '" + password + "')");
            return true;

        } else {
            info("Пароли не совпадают");
        }

        return false;
    }

    private void close() {
        regStage.close();
    }
}
