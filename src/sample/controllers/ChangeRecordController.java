package sample.controllers;

import constants.Constants;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ChangeRecordController implements Constants {

    @FXML
    private TextField numRecord;

    @FXML
    private Button selectBtn;

    @FXML
    private VBox vBox;

    private ArrayList<TextField> textFields;
    private ArrayList<String> columnNames;

    private String tableName;

    private DataBaseEditorController dbec;

    private Button changeBtn;

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setDbec(DataBaseEditorController dbec) {
        this.dbec = dbec;
    }

    @FXML
    private void initialize() throws SQLException {
        textFields = new ArrayList<>();
        changeBtn = new Button("Изменить");
        changeBtn.setFont(Font.font(17));

        selectBtn.setOnAction(actionEvent -> {
            try {
                generateForm();
                addValueTextField(Integer.parseInt(numRecord.getText()));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        changeBtn.setOnAction(actionEvent -> {
            try {
                DATA_BASE.changeRow(tableName, columnNames, getValues(), Integer.parseInt(numRecord.getText()));
                dbec.update();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void generateForm() throws SQLException {
        columnNames = DATA_BASE.getColumnNames(tableName, DATA_BASE.countColumns(tableName));
        ArrayList<Label> labels = createLabels(columnNames);
        createTextFields(columnNames.size());

        for (int i = 0; i < columnNames.size(); i++) {
            vBox.getChildren().add(labels.get(i));
            vBox.getChildren().add(textFields.get(i));
        }

        VBox vbox2 = new VBox();
        vbox2.setPrefHeight(50);
        vBox.getChildren().addAll(vbox2, changeBtn);
    }

    private ArrayList<Label> createLabels(ArrayList<String> columnNames) {
        ArrayList<Label> labels = new ArrayList<>();
        for (String columnName : columnNames) {
            Label label = new Label();
            label.setText(columnName);
            label.setTextAlignment(TextAlignment.LEFT);
            label.setFont(Font.font(17));
            labels.add(label);
        }

        return labels;
    }

    private void createTextFields(int columnsCount) {
        for (int i = 0; i < columnsCount; i++) {
            TextField textField = new TextField();
            textField.setAlignment(Pos.CENTER);
            textFields.add(textField);
        }
    }

    private ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();

        for (TextField textField : textFields) {
            values.add(textField.getText());
        }

        return values;
    }

    private void addValueTextField(int numRecord) throws SQLException {
        ResultSet rs = DATA_BASE.request("SELECT * FROM `" + tableName + "`");
        while (rs.next()) {
            if (rs.getInt(1) == numRecord) {
                for (int i = 1; i <= textFields.size(); i++) {
                    textFields.get(i - 1).setText(rs.getString(i));
                }
            }
        }
        rs.close();
    }
}
