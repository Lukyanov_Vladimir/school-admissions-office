package sample.controllers;

import constants.Constants;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.MapValueFactory;;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class DataBaseEditorController implements Constants {
    @FXML
    private ComboBox<String> tables;

    @FXML
    private Button addRecordBtn;

    @FXML
    private Button removeRecordBtn;

    @FXML
    private Button changeRecordBtn;

    @FXML
    private Button changeValueRecordBtn;

    @FXML
    private TableView<HashMap> tableView;

    @FXML
    private Label login;

    @FXML
    private ComboBox<String> tasks;

    private int columnCount;

    private ArrayList<String> tablesName;
    private ArrayList<String> tasksName;

    private Stage stage;

    private EventHandler<WindowEvent> eventHandler = new EventHandler<WindowEvent>() {
        @Override
        public void handle(WindowEvent e) {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/sample/fxml/main.fxml"));
                Parent root = fxmlLoader.load();
                Stage primaryStage = new Stage();
                primaryStage.setTitle("Главное меню");
                primaryStage.centerOnScreen();
                primaryStage.setResizable(false);
                primaryStage.setScene(new Scene(root));
                primaryStage.show();

                MainController mainController = fxmlLoader.getController();
                mainController.setMainStage(primaryStage);
                primaryStage.setOnCloseRequest(mainController.getEventHandler());
                DATA_BASE.close();

            } catch (SQLException | IOException ex) {
                ex.printStackTrace();
            }
        }
    };

    public EventHandler<WindowEvent> getEventHandler() {
        return eventHandler;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void initialize() {
        try {
            DATA_BASE.connect();
            tableView.getColumns().clear();
            tablesName = viewTables();
            tables.setItems(FXCollections.observableArrayList(tablesName));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        tasksName = new ArrayList<>();
        tasksName.add("Поданные заявления");
        tasksName.add("Абитуриенты получивших неудовлетворительно");
        tasksName.add("Абитуриенты не прошедшие по конкурсу");
        tasks.setItems(FXCollections.observableArrayList(tasksName));

        handleClicks();
    }

    public void setLogin(String login) {
        this.login.setText("Добро пожаловать: " + login);
    }

    private void showDataBaseTableInfo(String tableName) throws SQLException {
        columnCount = DATA_BASE.countColumns(tableName);

        ArrayList<String> columnNames = DATA_BASE.getColumnNames(tableName, columnCount);
        ArrayList<TableColumn> tableColumns = createTableColumns(columnNames);

        setItemsInTable(columnNames, tableName);
        setValuesColumns(tableColumns, columnNames);
        addColumnsInTable(tableColumns);
    }

    private ArrayList<TableColumn> createTableColumns(ArrayList<String> columnNames) {
        ArrayList<TableColumn> tableColumns = new ArrayList<>();
        for (int i = 0; i < columnNames.size(); i++) {
            TableColumn tableColumn = new TableColumn<HashMap, Object>(columnNames.get(i));
            tableColumn.setStyle("-fx-alignment: CENTER;");
            tableColumns.add(tableColumn);
        }
        return tableColumns;
    }

    public void update() {
        tableView.getColumns().clear();
        try {
            showDataBaseTableInfo(tables.getValue());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void setItemsInTable(ArrayList<String> columnNames, String tableName) throws SQLException {
        tableView.setItems(DATA_BASE.getInfo(tableName, columnCount, columnNames));
    }

    private void addColumnsInTable(ArrayList<TableColumn> tableColumns) {
        for (int i = 0; i < tableColumns.size(); i++) {
            tableView.getColumns().add(tableColumns.get(i));
        }
    }

    private void setValuesColumns(ArrayList<TableColumn> tableColumns, ArrayList<String> columnNames) {
        for (int i = 0; i < tableColumns.size(); i++) {
            tableColumns.get(i).setCellValueFactory(new MapValueFactory<String>(columnNames.get(i)));
        }
    }

    private ArrayList<String> viewTables() {
        ArrayList<String> tables = new ArrayList<>();

        try {
            ResultSet rs = DATA_BASE.request("SHOW TABLES");
            while (rs.next()) {
                tables.add(rs.getString(1));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tables;
    }

    private void displaySubmittedApplications() throws SQLException {
        tableView.getColumns().clear();

        TableColumn tableColumn = new TableColumn<HashMap, Object>("поданные заявления");
        tableColumn.setStyle("-fx-alignment: CENTER;");
        tableView.setItems(FXCollections.observableArrayList(DATA_BASE.readData(1, "поданные_заявления", "SELECT `наименование_специальности` FROM `специальности` ORDER BY `поданные_заявления` DESC")));
        tableColumn.setCellValueFactory(new MapValueFactory<String>("поданные_заявления"));
        tableView.getColumns().add(tableColumn);
    }

    private void handleClicks() {
        tables.setOnAction(event -> {
            try {
                tableView.getColumns().clear();
                showDataBaseTableInfo(tables.getValue());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        tasks.setOnAction(event -> {
            switch (tasks.getValue()) {
                case "Поданные заявления":
                    try {
                        displaySubmittedApplications();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    break;

                case "Абитуриенты получивших неудовлетворительно":
                    tableView.getColumns().clear();
                    ArrayList<String> columansNames = new ArrayList();
                    columansNames.add("ФИО_абитуриента");
                    columansNames.add("год_рождения");
                    columansNames.add("код_группы");
                    ArrayList<TableColumn> tableSpecialityColumnsArray = createTableColumns(columansNames);
                    try {
                        tableView.setItems(FXCollections.observableArrayList(DATA_BASE.readData(3, columansNames, "SELECT ФИО_абитуриента,год_рождения,код_группы FROM абитуриенты WHERE полученная_оценка='2'")));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    setValuesColumns(tableSpecialityColumnsArray, columansNames);
                    addColumnsInTable(tableSpecialityColumnsArray);
                    break;
                case "Абитуриенты не прошедшие по конкурсу":
                    try {
                        tableView.getColumns().clear();
                        ArrayList<String> columnsNames = new ArrayList();
                        int points = 0;
                        int i = 0;
                        ResultSet rs = DATA_BASE.request("SELECT баллы FROM looser");
                        while (rs.next()) {
                            i++;
                            points = points + rs.getInt("баллы");
                        }
                        int result = points / i;

                        columnsNames.add("ФИО_абитуриента");
                        ArrayList<TableColumn> tableSpecsialityColumnsArray = createTableColumns(columnsNames);
                        tableView.setItems(FXCollections.observableArrayList(DATA_BASE.readData(1, columnsNames, "SELECT ФИО_абитуриента FROM looser WHERE баллы>" + result)));
                        setValuesColumns(tableSpecsialityColumnsArray, columnsNames);
                        addColumnsInTable(tableSpecsialityColumnsArray);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        });

        addRecordBtn.setOnAction(actionEvent -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/sample/fxml/addRecord.fxml"));
                Parent root = fxmlLoader.load();
                Stage addRecord = new Stage();
                addRecord.setResizable(false);
                addRecord.setTitle("Добавление записи");
                addRecord.initModality(Modality.WINDOW_MODAL);
                addRecord.setScene(new Scene(root));
                addRecord.show();

                AddRecordController controller = fxmlLoader.getController();
                controller.setTableName(tables.getValue());
                controller.setDbec(this);
                try {
                    controller.generateForm();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        removeRecordBtn.setOnAction(actionEvent -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/sample/fxml/removeRecord.fxml"));
                Parent root = fxmlLoader.load();
                Stage removeRecord = new Stage();
                removeRecord.setResizable(false);
                removeRecord.setTitle("Удаление записи");
                removeRecord.initModality(Modality.WINDOW_MODAL);
                removeRecord.setScene(new Scene(root));
                removeRecord.show();

                RemoveRecordController controller = fxmlLoader.getController();
                controller.setTableName(tables.getValue());
                controller.setDbec(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        changeValueRecordBtn.setOnAction(actionEvent -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/sample/fxml/changeValueRecord.fxml"));
                Parent root = fxmlLoader.load();
                Stage changeValueRecord = new Stage();
                changeValueRecord.setResizable(false);
                changeValueRecord.setTitle("Изменение ячейки");
                changeValueRecord.initModality(Modality.WINDOW_MODAL);
                changeValueRecord.setScene(new Scene(root));
                changeValueRecord.show();

                ChangeValueRecordController controller = fxmlLoader.getController();
                controller.setTableName(tables.getValue());
                controller.setDbec(this);
                controller.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        changeRecordBtn.setOnAction(actionEvent -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/sample/fxml/changeRecord.fxml"));
                Parent root = fxmlLoader.load();
                Stage changeRecord = new Stage();
                changeRecord.setResizable(false);
                changeRecord.setTitle("Редактирование записи");
                changeRecord.initModality(Modality.WINDOW_MODAL);
                changeRecord.setScene(new Scene(root));
                changeRecord.show();

                ChangeRecordController controller = fxmlLoader.getController();
                controller.setTableName(tables.getValue());
                controller.setDbec(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
