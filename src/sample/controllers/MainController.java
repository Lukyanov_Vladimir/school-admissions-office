package sample.controllers;

import constants.Constants;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.sql.SQLException;

public class MainController implements Constants {

    @FXML
    private Button loginBtn;

    @FXML
    private Button exitBtn;

    private LoginController loginController;

    private Stage mainStage;

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    private EventHandler<WindowEvent> eventHandler = new EventHandler<WindowEvent>() {
        @Override
        public void handle(WindowEvent e) {
            try {
                DATA_BASE.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                mainStage.close();
            }
        }
    };

    public EventHandler<WindowEvent> getEventHandler() {
        return eventHandler;
    }

    @FXML
    public void initialize() {
        loginBtn.setCursor(Cursor.HAND);
        exitBtn.setCursor(Cursor.HAND);

        loginBtn.setOnAction(event -> {
            try {
                DATA_BASE.connect();

                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/sample/fxml/login.fxml"));
                Parent root = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setTitle("Вход");
                stage.setResizable(false);
                stage.initModality(Modality.WINDOW_MODAL);
                stage.centerOnScreen();
                stage.setScene(new Scene(root));
                stage.show();

                loginController = fxmlLoader.getController();
                loginController.setParent(this);
                loginController.setStage(stage);
                stage.setOnCloseRequest(loginController.getEventHandler());

            } catch (IOException | SQLException e) {
                e.printStackTrace();
            }
        });

        exitBtn.setOnAction(event -> {
            try {
                DATA_BASE.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                close();
            }
        });
    }

    public void close() {
        mainStage.close();
    }
}
