package sample.controllers;

import constants.Constants;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginController implements Constants {

    @FXML
    private TextField logField;

    @FXML
    private PasswordField passField;

    @FXML
    private Button logBtn;

    @FXML
    private Label labelReg;

    private MainController parent;

    private Stage loginStage;

    private RegistrationController registrationController;

    private String login;

    public void setParent(MainController parent) {
        this.parent = parent;
    }

    public void setStage(Stage loginStage) {
        this.loginStage = loginStage;
    }

    private EventHandler<WindowEvent> eventHandler = e -> {
        try {
            DATA_BASE.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    };

    public EventHandler<WindowEvent> getEventHandler() {
        return eventHandler;
    }

    @FXML
    public void initialize() {
        logBtn.setCursor(Cursor.HAND);
        labelReg.setCursor(Cursor.HAND);

        labelReg.setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.PRIMARY) {
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/sample/fxml/registration.fxml"));
                    Parent root = fxmlLoader.load();
                    Stage stage = new Stage();
                    stage.setTitle("Регистрация");
                    stage.setResizable(false);
                    stage.centerOnScreen();
                    stage.initModality(Modality.WINDOW_MODAL);
                    stage.setScene(new Scene(root));
                    stage.show();

                    registrationController = fxmlLoader.getController();
                    registrationController.setStage(stage);
                    stage.setOnCloseRequest(registrationController.getEventHandler());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        logBtn.setOnAction(event -> {
            try {
                DATA_BASE.connect();
                login = logField.getText();
                String password = passField.getText();
                if (login(login, password)) {
                    info("Поздравляем, вы успешно вошли");
                    openMainApp();
                    close();
                    parent.close();
                }
            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        });
    }

    public boolean login(String login, String password) throws SQLException {

        ResultSet rs = DATA_BASE.request("SELECT * FROM `" + TABLE_USERS + "` WHERE `login` = '" + login + "' AND `password` = '" + password + "'");

        if (rs.absolute(1)) {
            rs.close();
            return true;
        }

        info("Неправильный логин или пароль");
        rs.close();
        return false;
    }

    private void info(String str) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Информация");
        alert.setHeaderText(null);
        alert.setContentText(str);
        alert.showAndWait();
    }

    private void close() {
        loginStage.close();
    }

    private void openMainApp() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/sample/fxml/dataBaseEditor.fxml"));
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        stage.setTitle("Редактор базы данных");
        stage.setResizable(false);
        stage.centerOnScreen();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.setScene(new Scene(root));
        stage.show();

        DataBaseEditorController dataBaseEditorController = fxmlLoader.getController();
        dataBaseEditorController.setStage(stage);
        dataBaseEditorController.setLogin(login);

        stage.setOnCloseRequest(dataBaseEditorController.getEventHandler());
    }
}
