package constants;

import dataBase.DataBase;

public interface Constants {
    DataBase DATA_BASE = new DataBase("jdbc:mysql://localhost/is?serverTimezone=Europe/Moscow&useSSL=false", "root", null);
    String TABLE_USERS = "users";
}
