package dataBase;

import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;

public class DataBaseTest {
    @Test
    public void connectAndClose() {
        DataBase database = new DataBase("jdbc:mysql://localhost/is?serverTimezone=Europe/Moscow&useSSL=false", "root", null);
        try {
            Assert.assertTrue(database.connect());
            Assert.assertTrue(database.close());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void addRow() {
        DataBase database = new DataBase("jdbc:mysql://localhost/is?serverTimezone=Europe/Moscow&useSSL=false", "root", null);
        try {
            database.connect();
            ArrayList<String> values = new ArrayList<>();
            values.add("255");
            values.add("vasya");
            values.add("13524");
            database.addRow("users", values);
            Assert.assertTrue(database.request("SELECT * FROM `users` WHERE `login` = '"+values.get(1)+"' AND `password` = '" + values.get(2) + "'").absolute(1));
            removeRow();
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void removeRow() {
        DataBase database = new DataBase("jdbc:mysql://localhost/is?serverTimezone=Europe/Moscow&useSSL=false", "root", null);
        try {
            database.connect();
            database.removeRow("users",255);
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}