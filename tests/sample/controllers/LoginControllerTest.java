package sample.controllers;

import dataBase.DataBase;
import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;

public class LoginControllerTest {

    @Test
    public void loginTest() {
        LoginController lc = new LoginController();
        try {
            DataBase database = new DataBase("jdbc:mysql://localhost/is?serverTimezone=Europe/Moscow&useSSL=false", "root", null);
            database.connect();
            boolean result = lc.login("admin", "admin");
            Assert.assertTrue(result);
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void noLoginTest() {
        LoginController lc = new LoginController();
        try {
            DataBase database = new DataBase("jdbc:mysql://localhost/is?serverTimezone=Europe/Moscow&useSSL=false", "root", null);
            database.connect();
            boolean result = lc.login("admin", "admina");
            Assert.assertFalse(result);
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}