package sample.controllers;

import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;

public class RegistrationControllerTest {

    @Test
    public void registrationTest() {
        RegistrationController rc = new RegistrationController();
        try {
            boolean result = rc.registration("admin", "admin", "admin");
            Assert.assertTrue(result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void noRegistrationTest() {
        RegistrationController rc = new RegistrationController();
        try {
            boolean result = rc.registration("admin", "adminf", "admin");
            Assert.assertFalse(result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}